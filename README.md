# randomMovieSelector
python 2.7 movie selector script for windows.

# Description of recursiveRename.py:
Recursively walk through each folder and file starting from the current directory and rename any of '(', ')', ' ' to '.' so that selectRandomMovie.py will work properly.

# Description of selectRandomMovie.py:
Randomly selects and plays a movie from the current directory. The current directory must contain only sub-directories with a playable movie in the first sub-directory level.
