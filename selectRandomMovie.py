# File: selectRandomMovie.py
# Description: Randomly selects and plays a movie from the current directory.
#   The current directory must contain only sub-directories with a playable
#   movie in the first sub-directory level.
# Creator: @afable
# Assumptions: This program assumes Python 2.7.2 is currently installed.

import os
import time
import random
import re

path = './'
listing = os.listdir(path)
listingSize = len(listing)

print """
-----------------------------------
  File: selectRandomMovie.py      |
  Creator: @afable                |
-----------------------------------
"""


print "Loading folders from this directory",
time.sleep(1)
print ".",
time.sleep(1)
print ".",
time.sleep(1)
print "."
time.sleep(1)
print '\n\n'

for folder in listing[1:]:
    print folder

while (1):
    randomFolder = random.randint(1, listingSize-1)
    movieFolder = listing[randomFolder]

    # default folders on external hard drive
    if (movieFolder != 'wbfs' and movieFolder != 'msdownld.tmp'):
        break

print ''
print "Movie folder selected:"
print movieFolder
time.sleep(3)

moviePath = path + movieFolder + '/'
movieFiles = os.listdir(moviePath)

movieFound = 0

for fileName in movieFiles:
    if re.match(r"(.*avi)|(.*IFO)|(.*mkv)|(.*mp4)|(.*mv4)|(.*VOB)", fileName):
        moviePath += fileName
        print moviePath
        print ''

        # cannot handle ' ', '(', or ')'
        moviePath = moviePath.replace('/', '\\')
        movieFound += 1
        if (os.system( moviePath )):
            time.sleep(180)
        exit(0)
    else:
        print fileName

if not (movieFound):
    print "A movie file could not be found from the folder."
    time.sleep(180)
    exit(1)

exit(0)
