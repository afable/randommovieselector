# File: recursiveRename.py
# Description: Recursively walk through each folder and file starting
#   from the current directory and rename any of '(', ')', ' ' to '.'
#   so that selectRandomMovie.py will work properly.
# Creator: @afable
# Assumptions: This program assumes Python 2.7.2 is currently installed.

import re, os

# Warn user and test for correct input
prompt = raw_input("Warning: Program will modify all files and folders in current \
directory and sub-directories. Enter \"understood\" to continue: ")
prompt.lower()
print ''
if (prompt != "understood"):
    print "Exiting program."
    exit(1)

# Top directory to start the recursive renaming
DIR = './'

listing = os.listdir(DIR)
for i in listing:
    print i

# Short function to rename all files
def rename_files(dir):
    for fname in os.listdir(dir):
        if (fname == "System Volume Information" or fname == '$RECYCLE.BIN'):
            continue
        elif re.search(r'(\()+|(\))+|(\ )|(\,)|(\.\.)+|(_-_)|(\.-\.)', fname):
            print fname
            
            oldName = os.path.join( dir, fname )
            newName = oldName.replace( '(', '.' )
            newName = newName.replace( ')', '.' )
            newName = newName.replace( ' ', '.' )
            newName = newName.replace( ',', '.' )
            newName = newName.replace( '_-_', '-' )
            newName = newName.replace( '.-.', '-' )
            newName = newName.replace( '....', '.')
            newName = newName.replace( '...', '.')
            newName = newName.replace( '..', '.')
            print oldName + ' => ' + newName
            os.rename( oldName, newName )

for root, dirs, files in os.walk(DIR):
    rename_files(root)
